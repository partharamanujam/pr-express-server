"use strict";

var os = require("os"),
    uuid = require("uuid"),
    redis = require("redis"),
    expressWinston = require("express-winston"),
    winston = require("pr-winston-child");

var defaults,
    loggerOptions = {
        "transports": [
            new winston.transports.Console({
                "colorize": true
            })
        ]
    },
    loggerContext = {
        "name": "express-server",
        "host": os.hostname()
    },
    logger = new winston.ChildLogger(loggerOptions, loggerContext);

defaults = {
    "winstonHttpLogger": {
        "winstonInstance": logger,
        "statusLevels": true,
        "requestWhitelist": [].concat(expressWinston.requestWhitelist, ["ID", "secure", "hostname", "ip"]),
        "responseWhitelist": [].concat(expressWinston.responseWhitelist, ["_headers"])
    },
    "toobusy": {
        "maxLag": 50,
        "interval": 500
    },
    "requestID": {
        "key": "ID",
        "generator": uuid.v4
    },
    "requestLogger": {
        "key": "log",
        "logger": logger
    },
    "compression": {}, // use defaults
    "bodyParser": {}, // use defaults
    "helmet": {
        "contentSecurityPolicy": {
            "directives": {
                "defaultSrc": ["'self'"],
                "scriptSrc": ["*.google-analytics.com"],
                "styleSrc": ["'unsafe-inline'"],
                "imgSrc": ["*.google-analytics.com"],
                "connectSrc": ["'none'"],
                "fontSrc": ["'none'"],
                "objectSrc": ["'none'"],
                "mediaSrc": ["'none'"],
                "frameSrc": ["'none'"]
            }
        },
        "dnsPrefetchControl": {},
        "frameguard": {
            "action": "deny"
        },
        "hidePoweredBy": {},
        "hpkp": false, // disable by default
        "hsts": {},
        "ieNoOpen": {}, // no options; just truthy
        "noCache": false, // disable by default
        "noSniff": {},
        "referrerPolicy": {
            "policy": "no-referrer"
        },
        "xssFilter": {}
    },
    "redisCookieSession": {
        "redisConfig": {
            "client": redis.createClient(),
            "prefix": "testsession:"
        },
        "sessionConfig": {
            "name": "express-server",
            "secret": "this-is-not-a-secret",
            "resave": false,
            "rolling": true,
            "saveUninitialized": false,
            "unset": "destroy",
            "cookie": {
                "path": "/",
                "httpOnly": true,
                "secure": false,
                "maxAge": 5000
            }
        }
    },
    "expressWs": {}, // no options; just truthy
    "wsMonitor": {}, // // use defaults
    "port": 8080
};

// exports
module.exports = defaults;