# pr-express-server

ExpressJS based server with useful middlewares

## Philosophy

Simple and ready-to-use express-server pre-loaded with commonly used middleware provided as a starting point for development

## Installation

```bash
$ npm install pr-express-server
```

## APIs

The module exposes a simple create-function and returns an ExpressJS app with pre-loaded middleware. These middleware can be configured via options passed to the function. The app also has updated methods to start/listen and stop/close the server.

```js
var server = require("pr-express-server");

// create server
var app = server(options);

// setup the business-login as would with an express-app
app.set(); // properties
app.use(); // handlers

// start
app.listen(callback);

// stop
app.close(callback);
```

Following is the list of middleware that are pre-loaded and configurable via options - you may refer to the documentation of the respective modules for options:

| Type     | Option-key | Documentation |
| :------- | :--------- | :------------ |
| Req/Res Logger | winstonHttpLogger | [express-winston](https://www.npmjs.com/package/express-winston#options) |
| Overload Protection | toobusy | [toobusy-js](https://www.npmjs.com/package/toobusy-js#tunable-parameters) |
| Request ID | requestID | key: name; generator: function|
| Request Logger | requestLogger | key: name; logger: logger-object |
| HTTP Compression | compression | [compression](https://www.npmjs.com/package/compression#options) |
| Req Body-parser | bodyParser | [express-body-parser](https://www.npmjs.com/package/pr-express-body-parser#apis) |
| Security Headers | helmet | [helmet](https://www.npmjs.com/package/helmet#how-it-works) |
| Cookie Session | redisCookieSession | [express-redis-session](https://www.npmjs.com/package/pr-express-redis-session#apis) |
| Websockets | expressWs | [express-ws](https://www.npmjs.com/package/pr-express-ws#apis) |
| Sockets Monitoring| wsMonitor | [ws-monitor](https://www.npmjs.com/package/pr-ws-monitor#apis) |
| Listening Port | port | http-port |

Refer to default options for reference at: [defaults.js](https://gitlab.com/partharamanujam/pr-express-server/blob/master/lib/defaults.js)

## Usage  

Refer to test [server.js](https://gitlab.com/partharamanujam/pr-express-server/blob/master/test/server.js) implementation for usage details.

## Test

```bash
$ npm install # inside pr-express-server
$ npm test
```
