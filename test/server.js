"use strict";

var server = require("../index"),
    expressWs = require("pr-express-ws");

var app = server();

// redis-session
app.post("/value",
    function createValue(req, res) {
        req.session.value = 1;
        res.end();
    }
);
app.put("/value",
    function incrementValue(req, res) {
        req.session.value++;
        res.end();
    }
);
app.get("/value",
    function getValue(req, res) {
        res.json({
            "value": req.session.value || 0 // eslint-disable-line no-magic-numbers
        });
    }
);
// body-parser
app.post("/body",
    function postVaue(req, res) {
        res.json({
            "type": req.get("Content-Type"),
            "value": req.body
        });
    }
);
app.post("/raw",
    function postVaue(req, res) {
        res.json({
            "type": req.get("Content-Type"),
            "value": new Buffer(req.body).toString()
        });
    }
);
app.post("/multipart",
    function postVaue(req, res) {
        var data = {};

        req.busboy.on("field", function onField(fieldname, val) {
            data[fieldname] = val;
        });
        req.busboy.on("finish", function onFinish() {
            res.json({
                "type": "multipart/form-data",
                "value": data
            });
        });
    }
);
// express-ws
app.get("/hello", function getHello(req, res) {
    res.end("hello");
});
app.get("/ws", expressWs.checkWebsocketUpgrade,
    function createWs(req, res) {
        res.upgradeWebsocket(function onWsUpgrade(websock) {
            if (!websock) {
                return;
            }
            websock.on("message", function onWsMessage(data) { // loopback
                websock.send(data);
            });
        });
    }
);

// export
module.exports = app;