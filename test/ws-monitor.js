"use strict";

var server = require("./server"),
    WebSocket = require("ws");

/* globals describe, beforeEach, afterEach, it */
describe("testing ws-monitor - takes a while...", function testExpressWs() {
    var ws;

    this.timeout(120000); // eslint-disable-line no-invalid-this, no-magic-numbers
    beforeEach(function before(done) {
        server.listen(done);
    });
    afterEach(function after(done) {
        server.close(done);
    });
    it("PING websocket", function pingExisting(done) {
        ws = new WebSocket("http://localhost:8080/ws");
        ws.on("ping", function onPing() {
            ws.close();
            done();
        });
    });
    it("CLOSE paused websocket", function pingExisting(done) {
        ws = new WebSocket("http://localhost:8080/ws");
        ws.on("close", function onClose() {
            done();
        });
        ws.on("open", function onClose() {
            ws.pause();
            setTimeout(function resume() { // eslint-disable-line max-nested-callbacks
                ws.resume();
            }, 90000); // eslint-disable-line no-magic-numbers
        });
    });
});