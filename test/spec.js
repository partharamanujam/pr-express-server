"use strict";

/* globals describe */

function importTest(name, path) {
    describe(name, function test() {
        require(path); // eslint-disable-line global-require
    });
}

describe("running all tests", function runAllTest() {
    importTest("loading redis-session tests", "./redis-session");
    importTest("loading body-parser tests", "./body-parser");
    importTest("loading express-ws tests", "./express-ws");
    importTest("loading ws-monitor tests", "./ws-monitor");
});