"use strict";

var server = require("./server"), // eslint-disable-line no-mixed-requires
    supertest = require("supertest"),
    request = supertest.agent(server);

/* globals describe, beforeEach, afterEach, it */
describe("testing express-redis-session", function testExpressRedisSession() { // eslint-disable-line max-statements
    this.timeout(10000); // eslint-disable-line no-invalid-this, no-magic-numbers
    beforeEach(function before(done) {
        server.listen(done);
    });
    afterEach(function after(done) {
        server.close(done);
    });
    it("GET value before creating", function getValue(done) {
        request
            .get("/value")
            .expect(200, { // eslint-disable-line no-magic-numbers
                "value": 0
            }, done);
    });
    it("POST/CREATE value", function createValue(done) {
        request
            .post("/value")
            .expect(200, done); // eslint-disable-line no-magic-numbers
    });
    it("GET value after creating", function getValue(done) {
        request
            .get("/value")
            .expect(200, { // eslint-disable-line no-magic-numbers
                "value": 1
            }, done);
    });
    it("PUT/INCREMENT value", function updateValue(done) {
        request
            .put("/value")
            .expect(200, done); // eslint-disable-line no-magic-numbers
    });
    it("GET value after increment", function getValue(done) {
        request
            .get("/value")
            .expect(200, { // eslint-disable-line no-magic-numbers
                "value": 2
            }, done);
    });
    it("PUT/INCREMENT value", function updateValue(done) {
        request
            .put("/value")
            .expect(200, done); // eslint-disable-line no-magic-numbers
    });
    it("GET value after increment", function getValue(done) {
        request
            .get("/value")
            .expect(200, { // eslint-disable-line no-magic-numbers
                "value": 3
            }, done);
    });
    it("GET value after cookie-expire", function getValue(done) {
        setTimeout(function afterExpire() {
            request
                .get("/value")
                .expect(200, { // eslint-disable-line no-magic-numbers
                    "value": 0
                }, done);
        }, 7500); // eslint-disable-line no-magic-numbers
    });
});